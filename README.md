<img  align="left" width="150" style="float: left;" src="https://www.upm.es/sfs/Rectorado/Gabinete%20del%20Rector/Logos/UPM/CEI/LOGOTIPO%20leyenda%20color%20JPG%20p.png">

<img  align="right" width="60" style="float: right;" src="http://www.dit.upm.es/figures/logos/ditupm-big.gif">

<br/><br/><br/><br/><br/>
<img  align="center" width="130" style="float: center;" src="WebContent/images/logo.jpg">
<br/><br/>

# ISST: Grupo 18: CommBike 

Los servicios de alquiler de bicicletas se est�n convirtiendo en una buena alternativa de movilidad en grandes ciudades para combatir la contaminaci�n. En este proyecto se dise�ar� un servicio inform�tico de gesti�n que controle un parque de bicicletas f�sicas sin estaciones fijas.
El servicio ofrecer� una aplicaci�n que permita a los usuarios localizar, alquilar y devolver bicicletas disponibles en sus proximidades, as� como una aplicaci�n de gesti�n del parque de bicicletas para ser empleada por los administradores.

## Funcionalidad del servicio
La empresa ofrecer� un conjunto de bicicletas distribuidas a lo largo de su zona geogr�fica de operaci�n. El servicio no utilizar� estaciones fijas, sino que las bicicletas podr�n estar estacionadas en cualquier punto permitido de la v�a p�blica. El servicio para los clientes mostrar� la ubicaci�n de las bicicletas disponibles cercanas al usuario, as� como permitir que los usuarios alquilen y devuelvan las bicicletas. 
Para desbloquear una bicicleta y alquilarla el usuario indicar� en el servicio el c�digo identificador de la bicicleta que desea utilizar (podr�a ser un c�digo QR). Recibida la petici�n y autenticado al usuario, el sistema desbloquear� el cerrojo f�sico para permitir al usuario circular con ella. De forma similar, una vez el cliente haya llegado a su destino y estacionado la bicicleta, realizar� una nueva solicitud al servicio, que activar� el candado electr�nico para bloquear la rueda e impedir su desplazamiento no autorizado.
El servicio operar� �nicamente dentro de una regi�n de cobertura predeterminada. La entrega de una bicicleta alquilada para finalizar el servicio s�lo tendr� �xito si la bicicleta se encuentra dentro de los l�mites de servicio autorizados.
Con el objetivo de evitar robos y p�rdidas de bicicletas, cada una dispone de un chip GPS que se emplea para actualizar la ubicaci�n de cada bicicleta, utilizando la conexi�n m�vil instalada en cada bici. 
El servicio calcular� el coste del alquiler en funci�n del tiempo transcurrido desde la reserva hasta la devoluci�n.
Adicionalmente el servicio presentar� una vista de administraci�n, que permitir� a�adir, modificar o eliminar informaci�n sobre las bicicletas gestionadas. Para facilitar la planificaci�n y gesti�n del servicio tambi�n se ofrecer� una vista global que resuma la distribuci�n geogr�fica del parque de bicicletas para poder realizar una mejor reorganizaci�n.

## Propuesta de realizaci�n

El proceso de desarrollo seguir� una metodolog�a �gil basada en Scrum. El desarrollo vendr� guiado por un proceso de an�lisis donde se identifiquen los requisitos que deben ser cubiertos, incluyendo aspectos no funcionales tales como la seguridad y gesti�n de la informaci�n almacenada. El prototipo desarrollado demostrar� la funcionalidad b�sica, y servir� para evaluar la viabilidad tecnol�gica, y presentar posibles v�as de evoluci�n en futuras iteraciones del proceso de desarrollo. 
Se propone implementar este caso como una aplicaci�n web con arquitectura en tres capas. Para la capa de presentaci�n se puede usar HTML5/Javascript, con la posibilidad de visualizar las bicicletas pr�ximas disponibles mediante un mapa. La informaci�n de usuarios, alquileres y bicicletas (incluyendo su estado y ubicaci�n) se almacenar� de forma persistente usando una base de datos relacional. La selecci�n de tecnolog�as espec�ficas y 11 arquitectura deber� ser justificada durante el proyecto, de acuerdo con los requisitos y al dominio de aplicaci�n. 
Para el modelado de la informaci�n es posible utilizar datos abiertos de servicios similares que se encuentran actualmente en funcionamiento, aunque no resulta obligatoria su utilizaci�n.


## Equipo de desarrollo
* Ignacio S�nchez (@IgnacioSanchezRamirez) - Product Owner
* Camilo Andr�s (@camilomzr) - Scrum Master
* Teresa Morales (@tmoralesleon) - Developer
* Luc�a Gasc� (@luciagasc) - Developer
* Jes�s Gonz�lez (@jgonzori3) - Developer

## Inicializar el proyecto
Debemos cargar todas las dependencias del proyecto Maven, para as� poder arrancar la BBDD.

```
Abrir el proyecto -> Maven -> Update Proyect
```
Desde un terminal debemos iniciar la BBDD h2 incluida en las dependencias Maven del proyecto. �Cuidado cambiar el nombre de usuario!

```
java -jar C:\Users\nombre_usuario\.m2\repository\com\h2database\h2\1.4.197\h2-1.4.197.jar 
```
