package es.upm.dit.isst.commBike.dao;

import java.util.Collection;
import org.hibernate.Session;
import es.upm.dit.isst.commBike.model.Ubicacion;

public class UbicacionDAOImplementation implements UbicacionDAO{
	
	private static UbicacionDAOImplementation UDI = null;
	private UbicacionDAOImplementation() {}
	public static UbicacionDAOImplementation getInstance() {
		if ( UDI == null )
			UDI = new UbicacionDAOImplementation();
		return UDI;
	}
	@Override
	public void create(Ubicacion ubicacion) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(ubicacion);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print(e);
		} finally {
			session.close();
		}
	}
	
	@Override
	public void delete(Ubicacion ubicacion) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(ubicacion);
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
	}
	
	@Override
	public Ubicacion readUser(int userId) {
		return null;
	}
	@Override
	public Ubicacion readBici(int biciId) {
		return null;
	}
	
	@Override
	public void update(Ubicacion ubicacion) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(ubicacion);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print(e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Ubicacion> readAll() {
		Session session = SessionFactoryService.get().openSession();
		Collection<Ubicacion> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Usuario").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
}