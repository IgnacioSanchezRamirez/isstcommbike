package es.upm.dit.isst.commBike.dao;

import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import es.upm.dit.isst.commBike.model.Alquiler;

public class AlquilerDAOImplementation implements AlquilerDAO{
	
	private static AlquilerDAOImplementation UDI = null;
	private AlquilerDAOImplementation() {}
	public static AlquilerDAOImplementation getInstance() {
		if ( UDI == null )
			UDI = new AlquilerDAOImplementation();
		return UDI;
	}
	@Override
	public Alquiler create(Alquiler alquiler) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(alquiler);
			session.getTransaction().commit();
			return alquiler;
		} catch (Exception e) {
			System.out.print(e);
			return null;
		} finally {
			session.close();
		}
	}
	
	@Override
	public void delete(Alquiler alquiler) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(alquiler);
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
	}
	
	@Override
	public Alquiler read(String email) {
		return null;
	}

	
	@Override
	public void update(Alquiler alquiler) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(alquiler);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print(e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Alquiler> readAll() {
		Session session = SessionFactoryService.get().openSession();
		List<Alquiler> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Alquiler").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Alquiler> readUser(String email) {
		// TODO Auto-generated method stub
		Session session = SessionFactoryService.get().openSession();
		List<Alquiler> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Alquiler select * where email = '"+email+"'").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
	
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Alquiler> readBici(String biciId) {
		// TODO Auto-generated method stub
		Session session = SessionFactoryService.get().openSession();
		List<Alquiler> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Alquiler select * where biciId = '"+biciId+"'").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Alquiler> readUserANDBici(String email,String biciId ){
		Session session = SessionFactoryService.get().openSession();
		List<Alquiler> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Alquiler select * where biciId = '"+biciId+"' and email ='"+email+"'").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
	
	
	@Override
	public Alquiler readUserANDBiciANDHoraInicio(String email,String biciId, int horaInicio ){
		Session session = SessionFactoryService.get().openSession();
		Alquiler uss = null;
		try {
			session.beginTransaction();
			uss =  (Alquiler) session.createQuery("from Alquiler select * where biciId = '"+biciId+"' and email ='"+email+"' and horaInicio ='"+horaInicio+"'").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}

	
	
}