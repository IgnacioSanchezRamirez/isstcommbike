package es.upm.dit.isst.commBike.dao;

import java.util.Collection;
import org.hibernate.Session;
import es.upm.dit.isst.commBike.model.Bicicleta;
import es.upm.dit.isst.commBike.model.Usuario;

public class BicicletaDAOImplementation implements BicicletaDAO{
	
	private static BicicletaDAOImplementation UDI = null;
	private BicicletaDAOImplementation() {}
	public static BicicletaDAOImplementation getInstance() {
		if ( UDI == null )
			UDI = new BicicletaDAOImplementation();
		return UDI;
	}
	@Override
	public Bicicleta create(Bicicleta bicicleta) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.save(bicicleta);
			session.getTransaction().commit();
			return bicicleta;
		} catch (Exception e) {
			System.out.print(e);
			return null;
		} finally {
			session.close();
		}
	}
	
	@Override
	public void delete(Bicicleta bicicleta) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(bicicleta);
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
	}
	
	@Override
	public Bicicleta read(String id) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			Bicicleta us = null;
			us = session.get(Bicicleta.class,  id);
			session.getTransaction().commit();
			session.close();
			return us;
		} catch(Exception e) {
			System.out.print(e);
			return null;
		}finally {
			session.close();
		}
	}

	
	@Override
	public void update(Bicicleta bicicleta) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(bicicleta);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print(e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public Collection<Bicicleta> readAll() {
		Session session = SessionFactoryService.get().openSession();
		Collection<Bicicleta> uss = null;
		try {
			session.beginTransaction();
			uss = session.createQuery("from Bicicleta").list();
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
		return uss;
	}
	@Override
	public Bicicleta read(int id) {
		// TODO Auto-generated method stub
		return null;
	}
}