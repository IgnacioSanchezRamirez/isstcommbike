package es.upm.dit.isst.commBike.dao;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.query.Query;

import es.upm.dit.isst.commBike.model.Usuario;

public class UsuarioDAOImplementation implements UsuarioDAO{
	
	private static UsuarioDAOImplementation UsuarioDAO;
	
	private UsuarioDAOImplementation() {}
	
	
	public static UsuarioDAOImplementation getInstance() {
		if ( UsuarioDAO == null )
			UsuarioDAO = new UsuarioDAOImplementation();
		return UsuarioDAO;
	}
	
	@Override
    public Usuario create(Usuario usuario) {
        Session session = SessionFactoryService.get().openSession();
        session.beginTransaction();
           try {
                   session.save(usuario);
           } catch(Exception e) {
        	   usuario = null;
           }
           session.getTransaction().commit();
           session.close();
           return usuario; 
    }
	
	@Override
	public void delete(Usuario usuario) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.delete(usuario);
			session.getTransaction().commit();
		} catch(Exception e) {
			System.out.print(e);
		}finally {
			session.close();
		}
	}
	
	
	@Override
	public Usuario read(String email) {
		// TODO Auto-generated method stub
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			Usuario us = null;
			us = session.get(Usuario.class,  email);
			session.getTransaction().commit();
			session.close();
			return us;
		} catch(Exception e) {
			System.out.print(e);
			return null;
		}finally {
			session.close();
		}
	}
	
//	@Override
//	public Usuario readEmail(String email) {
//		Session session = SessionFactoryService.get().openSession();
//		try {
//			session.beginTransaction();
//			Usuario us = null;
//			us = session.get(Usuario.class,  email);
//			session.getTransaction().commit();
//			session.close();
//			return us;
//		} catch(Exception e) {
//			System.out.print(e);
//			return null;
//		}finally {
//			session.close();
//		}
//	}
//	
//	@Override
//	public Usuario readNombre(String nombre) {
//		Session session = SessionFactoryService.get().openSession();
//		try {
//			session.beginTransaction();
//			Usuario us = null;
//			us = session.get(Usuario.class,  nombre);
//			session.getTransaction().commit();
//			session.close();
//			return us;
//		} catch(Exception e) {
//			System.out.print(e);
//			return null;
//		}finally {
//			session.close();
//		}
//	}
	
	@Override
	public void update(Usuario usuario) {
		Session session = SessionFactoryService.get().openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(usuario);
			session.getTransaction().commit();
		} catch (Exception e) {
			System.out.print(e);
		} finally {
			session.close();
		}
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Usuario> readAll() {
		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();
		
		List<Usuario> client = null;
		
		client = session.createQuery("from Usuario").list();
		session.getTransaction().commit();
		session.close();
	
		return client;
	}
	
	
	@Override
	public Usuario login(String correo, String password) {

		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();

		Usuario us = null;
		us = (Usuario) session.createQuery("select u from Usuario u where u.correo = :correo and u.password = :password").setParameter("password", password).setParameter("correo", correo).uniqueResult();
			
		
		session.getTransaction().commit();
		session.close();
		
		return us;
	}

	@Override
	public Usuario nuevaTarjeta(String correo, String tarjeta) {

		Session session = SessionFactoryService.get().openSession();
		session.beginTransaction();

		Usuario pas = null;
		pas = (Usuario) session.createQuery("select u from Usuario u where u.correo = :correo").setParameter("tarjeta", tarjeta).uniqueResult();
			
		
		session.getTransaction().commit();
		session.close();
		
		return pas;
	}
	
}