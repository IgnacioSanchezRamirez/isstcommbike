package es.upm.dit.isst.commBike.dao;

import java.util.Collection;

import es.upm.dit.isst.commBike.model.Bicicleta;

public interface BicicletaDAO {
	//repasar metodo read
	public Bicicleta create(Bicicleta ubicacion);
	public Bicicleta read(int id);
	public void update(Bicicleta ubicacion);
	public void delete(Bicicleta ubicacion);
	public Collection<Bicicleta> readAll();
	public Bicicleta read(String id);
	
}

