package es.upm.dit.isst.commBike.dao;

import java.util.Collection;

import es.upm.dit.isst.commBike.model.Ubicacion;

public interface UbicacionDAO {
	//repasar metodo read
	public void create(Ubicacion ubicacion);
	public Ubicacion readUser(int userId);
	public Ubicacion readBici(int biciId);
	public void update(Ubicacion ubicacion);
	public void delete(Ubicacion ubicacion);
	public Collection<Ubicacion> readAll();
	
}

