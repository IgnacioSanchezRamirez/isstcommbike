package es.upm.dit.isst.commBike.dao;

import java.util.Collection;
import java.util.List;

import es.upm.dit.isst.commBike.model.Alquiler;

public interface AlquilerDAO {
	//repasar metodo read
	public Alquiler create(Alquiler alquiler);
	public Alquiler read(String email);
	public List<Alquiler> readUser(String email);
	public List<Alquiler> readBici(String biciId);
	public List<Alquiler> readUserANDBici(String email,String biciId );
	public Alquiler readUserANDBiciANDHoraInicio(String email,String biciId,int horaInicio);
	public void update(Alquiler alquiler);
	public void delete(Alquiler alquiler);
	public List<Alquiler> readAll();
	
}

