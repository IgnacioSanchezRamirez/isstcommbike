package es.upm.dit.isst.commBike.dao;

import java.util.Collection;
import java.util.List;

import es.upm.dit.isst.commBike.model.Usuario;

public interface UsuarioDAO {
	
	public Usuario create(Usuario usuario);
	public Usuario read(String email);
	public void update(Usuario usuario);
	public void delete(Usuario usuario);
	public List<Usuario> readAll();
	public Usuario login(String correo, String password);
//	public Usuario readEmail(String email);
//	public Usuario readNombre(String nombre);
	public Usuario nuevaTarjeta(String correo, String tarjeta);
	
}

