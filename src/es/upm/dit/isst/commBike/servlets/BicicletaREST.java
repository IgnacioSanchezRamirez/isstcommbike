package es.upm.dit.isst.commBike.servlets;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Collection;

import javax.ws.rs.Consumes;
import javax.ws.rs.FormParam;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.isst.commBike.dao.BicicletaDAO;
import es.upm.dit.isst.commBike.dao.BicicletaDAOImplementation;
import es.upm.dit.isst.commBike.dao.UsuarioDAO;
import es.upm.dit.isst.commBike.dao.UsuarioDAOImplementation;
import es.upm.dit.isst.commBike.model.Bicicleta;

@Path("/bicicletas")
public class BicicletaREST {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public Collection<Bicicleta> readAll () {
	    return BicicletaDAOImplementation.getInstance().readAll();
	}

	@GET
	@Path("{id}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response read(@PathParam("id") int id) {
		Bicicleta c = BicicletaDAOImplementation.getInstance().read(id);
	
	    if (c == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	
	    return Response.ok(c, MediaType.APPLICATION_JSON).build();
	 }
	
//	@POST
//	@Produces(MediaType.TEXT_PLAIN)
//	public String crear(@FormParam("idAsignado") String idAsignado) {
//	  System.out.println(idAsignado);
//	  Bicicleta bici = new Bicicleta();
//		//Creamos el usuario con los datos introducidos en la Web.
//	  bici.setId("342");
//	  bici.setAlquilada("2");
//
//		//Ahora, metemos en la base de datos 
//	    BicicletaDAO bicidao = BicicletaDAOImplementation.getInstance();
//	    bicidao.create(bici);
//	  
//
//	  return "123";
//	 }
	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	@Path("{id}")
//	public Response update(@PathParam("id") String id, Bicicleta b) {
//		System.out.println("Update request for" + id + " " + b.toString());
//        Bicicleta told = BicicletaDAOImplementation.getInstance().read(id);
//        if ((told == null) || (! told.getId().contentEquals(b.getId())))
//          return Response.notModified().build();
//        BicicletaDAOImplementation.getInstance().update(b);
//        return Response.ok().build();
//	}
//	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response create(Bicicleta bnew) throws URISyntaxException {
//	    Bicicleta t = BicicletaDAOImplementation.getInstance().create(bnew);
//	    if (t != null) {
//	            URI uri = new URI("/TFG-SERVICE/api/bicicletas" + t.getId());
//	            return Response.created(uri).build();
//	    }
//	    return Response.status(Response.Status.NOT_FOUND).build();
//
//	}

}
