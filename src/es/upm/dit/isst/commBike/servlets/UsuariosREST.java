package es.upm.dit.isst.commBike.servlets;


import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.PATCH;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import es.upm.dit.isst.commBike.dao.UsuarioDAO;
import es.upm.dit.isst.commBike.dao.UsuarioDAOImplementation;
import es.upm.dit.isst.commBike.model.Usuario;

@Path("/usuarios")
public class UsuariosREST {
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Usuario> readAll () {
	    return UsuarioDAOImplementation.getInstance().readAll();
	}
//
//	@GET
//	@Path("{email}")
//	@Produces(MediaType.APPLICATION_JSON)
//	
//	public Response readEmail(@PathParam("email") String email) {
//		try {
//			Usuario c = UsuarioDAOImplementation.getInstance().readEmail(email);
//			
//		    if (c == null)
//		      return Response.status(Response.Status.NOT_FOUND).build();
//		
//		    return Response.ok(c, MediaType.APPLICATION_JSON).build();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			return readEmail(email);
//		}
//		
//	 }  

	@GET
	@Path("{email}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response read(@PathParam("email") String email) {
		Usuario c = UsuarioDAOImplementation.getInstance().read(email);
	
	    if (c == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	
	    return Response.ok(c, MediaType.APPLICATION_JSON).build();
	 }     

	
	
//	@GET
//	@Path("{nombre}")
//	@Produces(MediaType.APPLICATION_JSON)
//	
//	public Response readNombre(@PathParam("nombre") String nombre) {
//		try {
//			Usuario c = UsuarioDAOImplementation.getInstance().readNombre(nombre);
//			
//		    if (c == null)
//		      return Response.status(Response.Status.NOT_FOUND).build();
//		
//		    return Response.ok(c, MediaType.APPLICATION_JSON).build();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			return readEmail(nombre);
//		}
//		
//	 }
//	
	
	@GET 
	@Path("{email}/{password}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response login(@PathParam("email") String email, @PathParam("password") String password){
		Usuario userLogin;
        System.out.println("has llamado a la api email password jeje");

		userLogin = UsuarioDAOImplementation.getInstance().login(email, password);
		
		System.out.println(userLogin);
		if(userLogin == null)
			return Response.ok(false, MediaType.APPLICATION_JSON).build();  
		
		return Response.ok(userLogin, MediaType.APPLICATION_JSON).build();                

	 }
	
	
	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response create(Usuario unew) throws URISyntaxException {
//	    Usuario t = UsuarioDAOImplementation.getInstance().create(unew);
//	    if (t != null) {
//	            URI uri = new URI("/TFG-SERVICE/api/usuarios/" + t.getCorreo());
//	            return Response.created(uri).build();
//	    }
//	    return Response.status(Response.Status.NOT_FOUND).build();
//
//	}
//	
	
//	@POST	
//	@Consumes({MediaType.APPLICATION_JSON})
//	@Produces({MediaType.APPLICATION_JSON})
//	@Path("/nuevoUsuario")
//	public Response nuevoUsuario(Usuario nuevo) {
//    
//		Usuario u;
//		
//		u = UsuarioDAOImplementation.getInstance().create(nuevo);
//		
//		System.out.println(u);
//		if(u == null)
//			return Response.ok(false, MediaType.APPLICATION_JSON).build();  
//		
//		return Response.ok(u, MediaType.APPLICATION_JSON).build();                
//
//	}
	
	
	 @POST
	 @Path("/nuevoUsuario")
     @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
     public Response create(Usuario tnew) throws URISyntaxException {
         Usuario t = UsuarioDAOImplementation.getInstance().create(tnew);
         
			/*
			 * UsuarioDAO t = UsuarioDAOImplementation.getInstance(); t.create(tnew);
			 */
 	    
         if (t != null) {
        	 
         URI uri = new URI("/isst-commbike/api/usuarios/" + t.getCorreo());
         //return Response.temporaryRedirect(uri).build();
         //return Response.created(uri).build();
         return Response.ok(t, MediaType.APPLICATION_JSON).build(); 
         }
         return Response.status(Response.Status.NOT_FOUND).build();
     }
	 
	 @PUT
	 @Path("/nuevaTarjeta/usuario={correo}")
     @Consumes(MediaType.APPLICATION_JSON)
	 @Produces(MediaType.APPLICATION_JSON)
     public Response addTarjeta(@PathParam("email") String email, Usuario tnew) throws URISyntaxException {
         Usuario t = UsuarioDAOImplementation.getInstance().create(tnew);
         Usuario told = UsuarioDAOImplementation.getInstance().read(email);
         
         if ((told == null) || (! told.getCorreo().contentEquals(t.getCorreo())))

             return Response.notModified().build();

           UsuarioDAOImplementation.getInstance().update(t);

           return Response.ok().build();
           
     }
	 
		
}