package es.upm.dit.isst.commBike.servlets;

import java.net.URI;
import java.net.URISyntaxException;
import java.sql.Date;
import java.util.Collection;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.h2.command.ddl.CreateAggregate;

import es.upm.dit.isst.commBike.dao.AlquilerDAOImplementation;
import es.upm.dit.isst.commBike.dao.UsuarioDAOImplementation;
import es.upm.dit.isst.commBike.model.Alquiler;
import es.upm.dit.isst.commBike.model.Bicicleta;
import es.upm.dit.isst.commBike.model.Usuario;


@Path("/alquiler")
public class AlquilerREST {
	
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	public List<Alquiler> readAll () {
	    return AlquilerDAOImplementation.getInstance().readAll();
	}
	
	/*
	 * @GET
	 * 
	 * @Path("{userid}/{biciid}")
	 * 
	 * @Produces(MediaType.APPLICATION_JSON)
	 * 
	 * public Response read(@PathParam("userid") String userid, @PathParam("biciid")
	 * String biciid ) { Float[] b =
	 * AlquilerDAOImplementation.getInstance().read(biciid).getUbicacion(); Float[]
	 * u = UsuarioDAOImplementation.getInstance().read(userid).getUbicacion(); Date
	 * d = new Date(0);
	 * 
	 * if (b==u) { Alquiler alquiler = new
	 * Alquiler("userid","biciid",d.toString(),null,null); }
	 * 
	 * if (b == null || u ==null) return
	 * Response.status(Response.Status.NOT_FOUND).build();
	 * 
	 * return Response.ok(b, MediaType.APPLICATION_JSON).build(); }
	 * 
	 * 
	 */
	
	
	
	/*como tinen un parametro string tanto readAlquilerUser como readAlquilerBici
	 *  y dependen de strings que cuelgan de /alquiler {email} y {biciId} que tambien 
	 *  son string hace el try y si no lo reconoce hace el catch que es la otra funcion
	 * */
//	@GET
//	@Path("{email}")
//	@Produces(MediaType.APPLICATION_JSON)
//	
//	public Response readAlquilerUser(@PathParam("email") String email) {
//		try {
//			List<Alquiler> c = AlquilerDAOImplementation.getInstance().readUser(email);
//			
//		    if (c == null)
//		      return Response.status(Response.Status.NOT_FOUND).build();
//		
//		    return Response.ok(c, MediaType.APPLICATION_JSON).build();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			return readAlquilerBici(email);
//		}
//		
//		
//	 }
	
	@GET
	@Path("{email}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response read(@PathParam("email") String email) {
		Alquiler c = AlquilerDAOImplementation.getInstance().read(email);
	
	    if (c == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	
	    return Response.ok(c, MediaType.APPLICATION_JSON).build();
	 }     

	
	
	/*como tinen un parametro string tanto readAlquilerUser como readAlquilerBici
	 *  y dependen de strings que cuelgan de /alquiler {email} y {biciId} que tambien 
	 *  son string hace el try y si no lo reconoce hace el catch que es la otra funcion
	 * */
	
//	@GET
//	@Path("{biciid}")
//	@Produces(MediaType.APPLICATION_JSON)
//	
//	public Response readAlquilerBici(@PathParam("biciid") String biciid) {
//		try {
//			List<Alquiler> c = AlquilerDAOImplementation.getInstance().readBici(biciid);
//			
//		    if (c == null)
//		      return Response.status(Response.Status.NOT_FOUND).build();
//		
//		    return Response.ok(c, MediaType.APPLICATION_JSON).build();
//			
//		} catch (Exception e) {
//			// TODO: handle exception
//			return readAlquilerUser(biciid);
//		}
//		
//	 }
	
	
	
	@GET
	@Path("{email}/{biciId}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response readAlquilerUserANDBiciResponse(@PathParam("email") String email,@PathParam("biciId") String biciId) {
		
		List<Alquiler> c = AlquilerDAOImplementation.getInstance().readUserANDBici(email,biciId);
		
	    if (c == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	
	    return Response.ok(c, MediaType.APPLICATION_JSON).build();
			
		
		
	 }
	
	@GET
	@Path("{email}/{biciId}/{horaInicio}")
	@Produces(MediaType.APPLICATION_JSON)
	
	public Response readAlquilerUserANDBiciResponse(@PathParam("email") String email,@PathParam("biciId") String biciId,@PathParam("horaInicio") int horaInicio) {
		
		Alquiler c = AlquilerDAOImplementation.getInstance().readUserANDBiciANDHoraInicio(email, biciId, horaInicio);
		
	    if (c == null)
	      return Response.status(Response.Status.NOT_FOUND).build();
	
	    return Response.ok(c, MediaType.APPLICATION_JSON).build();
			
		
		
	 }
	
	
//	@POST
//	@Consumes(MediaType.APPLICATION_JSON)
//	public Response create(Alquiler a) throws URISyntaxException {
//	    Alquiler t = AlquilerDAOImplementation.getInstance().create(a);
//	    if (t != null) {
//	            URI uri = new URI("/TFG-SERVICE/api/alquiler" + t.getUserId());
//	            return Response.created(uri).build();
//	    }
//	    return Response.status(Response.Status.NOT_FOUND).build();
//
//	}
	
	

}
