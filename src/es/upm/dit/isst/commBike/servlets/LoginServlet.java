package es.upm.dit.isst.commBike.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.core.MediaType;

import org.glassfish.jersey.client.ClientConfig;

import es.upm.dit.isst.commBike.model.*;
import es.upm.dit.isst.commBike.dao.*;

@WebServlet({ "/LoginServlet"})
public class LoginServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//ProfessorDAO pdao = ProfessorDAOImplementation.getInstance();
		//req.getSession().setAttribute( "professor_list", pdao.readAll() );
		String email = req.getParameter( "email" );
		String pass = req.getParameter( "txtPassword" );
		System.out.println(email);
		System.out.println(pass);
//		//Empresa empresa = EmpresaDAOImplementation.getInstance().readEmpresa(nombreEmpresa);
//		
//		
//		//Creamos un nuevo objeto trabajador
//		Usuario usuario = new Usuario();
//		
//		//Creamos el usuario con los datos introducidos en la Web.
//		usuario.setCorreo(email);
//		usuario.setTarjeta("123123");
//		usuario.setPassword(pass);
//
//		//Ahora, metemos en la base de datos 
//	    UsuarioDAO userdao = UsuarioDAOImplementation.getInstance();
//	    userdao.create(usuario);
//	    
//	    req.getSession().setAttribute("userType", "user");
//	    req.getSession().setAttribute("email", email);
//	    //resp.sendRedirect(req.getContextPath() + "/admin.jsp");
//			
//		getServletContext().getRequestDispatcher( "/mapaAdmin.jsp" ).forward( req, resp );
		
		
		
		// autenticacion3
        Client client = ClientBuilder.newClient(new ClientConfig());
        
        Usuario us = null;
        System.out.println(URLHelper.getURL());
        us = client.target(URLHelper.getURL()+"/usuarios/" + email +"/" + pass).request().accept(MediaType.APPLICATION_JSON).get(Usuario.class);


        System.out.println("El maldito usuario:");
        System.out.println(us);
        
        if ( null != us ) {

                req.getSession().setAttribute("usuario2", us);

                req.setAttribute("usuario", us);
                req.setAttribute("nombre", us.getNombre());
                req.getSession().setAttribute("nombre2", us.getNombre());
                req.setAttribute("correo", us.getCorreo());

                getServletContext().getRequestDispatcher("/mapa.jsp").forward(req,resp);

                return;

        }

        getServletContext().getRequestDispatcher("/misDatos.jsp").forward(req,resp);
	}
	
	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

			resp.sendRedirect( req.getContextPath() + "/LoginPost" );
			
	}
}
