package es.upm.dit.isst.commBike.servlets;

import java.io.IOException;  
import java.io.PrintWriter;  
  
import javax.servlet.ServletException;  
import javax.servlet.http.HttpServlet;  
import javax.servlet.http.HttpServletRequest;  
import javax.servlet.http.HttpServletResponse;  
import javax.servlet.http.HttpSession;  
public class LogoutServlet extends HttpServlet {  
        /**
	 * 
	 */
	private static final long serialVersionUID = 1L;

		/*protected void doGet(HttpServletRequest request, HttpServletResponse response)  throws ServletException, IOException {  
            response.setContentType("/login.jsp");  
            PrintWriter out=response.getWriter();  
              
            request.getRequestDispatcher("/login.jsp").include(request, response);
            
            HttpSession session=request.getSession();  
            session.invalidate();
            
            out.print("You are successfully logged out!");  
            out.close();
		}  */
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        	req.getSession().invalidate();
        	getServletContext().getRequestDispatcher("/login.jsp").forward(req,resp);
        }
}

