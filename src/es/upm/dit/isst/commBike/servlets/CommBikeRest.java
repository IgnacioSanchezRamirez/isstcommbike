package es.upm.dit.isst.commBike.servlets;

import javax.ws.rs.ApplicationPath; 
import javax.ws.rs.core.Application; 

@ApplicationPath("api") 
public class CommBikeRest extends Application { 
}
