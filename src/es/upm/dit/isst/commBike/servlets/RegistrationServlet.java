package es.upm.dit.isst.commBike.servlets;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import es.upm.dit.isst.commBike.model.*;
import es.upm.dit.isst.commBike.dao.*;

@WebServlet({ "/RegistrationServlet"})
public class RegistrationServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	@Override
	  protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		//ProfessorDAO pdao = ProfessorDAOImplementation.getInstance();
		//req.getSession().setAttribute( "professor_list", pdao.readAll() );
		String nombre = req.getParameter( "nombre" ).trim();
		String email = req.getParameter( "email" ).trim();
		String pass = req.getParameter( "password" ).trim();
		
		//Empresa empresa = EmpresaDAOImplementation.getInstance().readEmpresa(nombreEmpresa);
		
		
		//Creamos un nuevo objeto trabajador
		Usuario usuario = new Usuario();
		
		//Creamos el usuario con los datos introducidos en la Web.
		usuario.setNombre(nombre);
		usuario.setCorreo(email);
		usuario.setTarjeta("123123");
		usuario.setPassword(pass);

		//Ahora, metemos en la base de datos 
	    UsuarioDAO userdao = UsuarioDAOImplementation.getInstance();
	    userdao.create(usuario);
	    
	    req.getSession().setAttribute("userType", "user");
	    req.getSession().setAttribute("email", email);
	    //resp.sendRedirect(req.getContextPath() + "/admin.jsp");
			
		getServletContext().getRequestDispatcher( "/login.jsp" ).forward( req, resp );
	}
	
}
