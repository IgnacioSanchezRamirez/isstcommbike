package es.upm.dit.isst.commBike.servlets;

public class URLHelper {
	public static String getURL() {
        String envValue = System.getenv("COMMBIKE_SRV_SERVICE_HOST");
        if(envValue == null)
                return "http://localhost:8080/isst-commbike/api";
        else
                return envValue;

}
}
