package es.upm.dit.isst.commBike.model;

import java.io.Serializable;

import javax.persistence.*;



//testing h2 - camilo commit
//import org.hibernate.annotations.LazyCollection;
//import org.hibernate.annotations.LazyCollectionOption;
//import java.util.Collection;

@Entity
public class Alquiler implements Serializable {
	
	
	@Id
	private String userId;
	private String biciId;
	private int horaInicio;
	private int horaFin;
	private String precio;

	private static final long serialVersionUID = 1L;


	public Alquiler(String userId, String biciId, int horaInicio, int horaFin, String precio) {
		super();
		this.userId = userId;
		this.biciId = biciId;
		this.horaInicio = horaInicio;
		this.horaFin = horaFin;
		this.precio = precio;
	}

	public Alquiler() {
		
	}
	
	public String getUserId() {
		return userId;
	}


	public void setUserId(String userId) {
		this.userId = userId;
	}


	public String getBiciId() {
		return biciId;
	}


	public void setBiciId(String biciId) {
		this.biciId = biciId;
	}


	public int getHoraInicio() {
		return horaInicio;
	}


	public void setHoraInicio(int horaInicio) {
		this.horaInicio = horaInicio;
	}


	public int getHoraFin() {
		return horaFin;
	}


	public void setHoraFin(int horaFin) {
		this.horaFin = horaFin;
	}


	public String getPrecio() {
		return precio;
	}


	public void setPrecio(String precio) {
		this.precio = precio;
	}



	
}
