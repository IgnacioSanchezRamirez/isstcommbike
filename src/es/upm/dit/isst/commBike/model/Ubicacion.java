package es.upm.dit.isst.commBike.model;

import java.io.Serializable;

import javax.persistence.*;



//testing h2 - camilo commit
//import org.hibernate.annotations.LazyCollection;
//import org.hibernate.annotations.LazyCollectionOption;
//import java.util.Collection;

@Entity
public class Ubicacion implements Serializable {

	@Id
	private String userId;
	private String ubicacionInicio;
	private String ubicacionFin;
	private String biciId;

	

	private static final long serialVersionUID = 1L;

	
	public Ubicacion(String userId, String ubicacionInicio, String ubicacionFin, String biciId) {
		super();
		this.userId = userId;
		this.ubicacionInicio = ubicacionInicio;
		this.ubicacionFin = ubicacionFin;
		this.biciId = biciId;
	}
	public Ubicacion() {
		
	}
	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}
	public String getUbicacionInicio() {
		return ubicacionInicio;
	}

	public void setUbicacionInicio(String ubicacionInicio) {
		this.ubicacionInicio = ubicacionInicio;
	}
	public String getUbicacionFin() {
		return ubicacionFin;
	}

	public void setUbicacionFin(String ubicacionFin) {
		this.ubicacionFin = ubicacionFin;
	}

	public String getBiciId() {
		return biciId;
	}

	public void setBiciId(String biciId) {
		this.biciId = biciId;
	}

}
