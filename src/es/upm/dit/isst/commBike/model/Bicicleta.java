package es.upm.dit.isst.commBike.model;

import java.io.Serializable;

import javax.persistence.*;

import org.hibernate.annotations.CollectionType;



//testing h2 - camilo commit
//import org.hibernate.annotations.LazyCollection;
//import org.hibernate.annotations.LazyCollectionOption;
//import java.util.Collection;

@Entity
public class Bicicleta implements Serializable {

	@Id
	private String id;
	private String alquilada;
	private String estado;
	private String fechaCreacion;
	private String datos;
	private Float[] ubicacion;

	
	private static final long serialVersionUID = 1L;


	public Bicicleta(String id, String alquilada, String estado, String fechaCreacion, String datos, Float[] ubicacion) {
		super();
		this.id = id;
		this.alquilada = alquilada;
		this.estado = estado;
		this.fechaCreacion = fechaCreacion;
		this.datos = datos;
		this.ubicacion = ubicacion;
	}



	public Bicicleta() {
		
	}
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	public String getAlquilada() {
		return alquilada;
	}

	public void setAlquilada(String alquilada) {
		this.alquilada = alquilada;
	}
	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getFechaCreacion() {
		return fechaCreacion;
	}

	public void setFechaCreacion(String fechaCreacion) {
		this.fechaCreacion = fechaCreacion;
	}
	public String getDatos() {
		return datos;
	}

	public void setDatos(String datos) {
		this.datos = datos;
	}
	
	public Float[] getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Float[] ubicacion) {
		this.ubicacion = ubicacion;
	}
	
}
