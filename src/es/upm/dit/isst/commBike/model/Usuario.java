package es.upm.dit.isst.commBike.model;

import java.io.Serializable;

import javax.persistence.*;



//testing h2 - camilo commit
//import org.hibernate.annotations.LazyCollection;
//import org.hibernate.annotations.LazyCollectionOption;
//import java.util.Collection;

@Entity
public class Usuario implements Serializable {

	@Id
	private String correo;
	private String nombre;
	private String password;
	private String tarjeta;
	private Float[] ubicacion;
	

	

	private static final long serialVersionUID = 1L;

	
	public Usuario() {
		
	}
	
	public Usuario(String nombre, String correo, String password, String tarjeta, Float[] ubicacion) {
		super();
		this.nombre = nombre;
		this.correo = correo;
		this.password = password;
		this.tarjeta = tarjeta;
		this.ubicacion = ubicacion;
	}
	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	public String getCorreo() {
		return correo;
	}

	public void setCorreo(String correo) {
		this.correo = correo;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getTarjeta() {
		return tarjeta;
	}

	public void setTarjeta(String tarjeta) {
		this.tarjeta = tarjeta;
	}
	
	public Float[] getUbicacion() {
		return ubicacion;
	}

	public void setUbicacion(Float[] ubicacion) {
		this.ubicacion = ubicacion;
	}

	/*
	 * @Override public String toString() { return "Usuarios [correo=" + correo +
	 * ", userId=" + userId + ", contrasena=" + contrasena + ", tarjeta=" + tarjeta
	 * + "]"; }
	 */
	
}
