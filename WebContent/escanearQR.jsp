<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>escanearQR - CommBike - Tu aplicación de movilidad rápida y sostenible.</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
    

	<link rel="stylesheet" href="css/reset.css">
	<link rel="stylesheet" href="css/styles.css">
	<script src="js/jquery.min.js"></script>
	<script src="js/webcam.min.js"></script>

	<script type="text/javascript" src="js/qr/grid.js"></script>
	<script type="text/javascript" src="js/qr/version.js"></script>
	<script type="text/javascript" src="js/qr/detector.js"></script>
	<script type="text/javascript" src="js/qr/formatinf.js"></script>
	<script type="text/javascript" src="js/qr/errorlevel.js"></script>
	<script type="text/javascript" src="js/qr/bitmat.js"></script>
	<script type="text/javascript" src="js/qr/datablock.js"></script>
	<script type="text/javascript" src="js/qr/bmparser.js"></script>
	<script type="text/javascript" src="js/qr/datamask.js"></script>
	<script type="text/javascript" src="js/qr/rsdecoder.js"></script>
	<script type="text/javascript" src="js/qr/gf256poly.js"></script>
	<script type="text/javascript" src="js/qr/gf256.js"></script>
	<script type="text/javascript" src="js/qr/decoder.js"></script>
	<script type="text/javascript" src="js/qr/qrcode.js"></script>
	<script type="text/javascript" src="js/qr/findpat.js"></script>
	<script type="text/javascript" src="js/qr/alignpat.js"></script>
	<script type="text/javascript" src="js/qr/databr.js"></script>

	<script src="js/effects.js"></script>
	
  </head>

  <header class="navbar navbar-expand-md navbar-dark d-print-none">
    <div class="container-xl">
      <jsp:include page="menu.jsp" flush="true" />
    </div>
  </header>

  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
      <div class="container-tight py-6">
        
        <form class="card card-md" action="." method="get" autocomplete="off">
        	<div class="card-body">
        		<h2 class="card-title text-center mb-4">Escanear código QR</h2>

				<div id="example" style="width:320px; height:240px;"></div>
        		
        		<div id="hiddenImg"></div>
        		<div class="mb-2">
					<button id="button" type="submit" class="btn btn-primary w-100" >Escanear</button>
        		</div>
        		
				<div class="form-footer">
					<div id="hiddenImg"></div>
					<label class="form-label">El id de la bicicleta asignada es:</label>
					<div id="qrContent"><p></p></div>
				</div>

        	</div>
        </form>
        
        
      
      </div>

    </div>
    
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
  </body>

</html>



