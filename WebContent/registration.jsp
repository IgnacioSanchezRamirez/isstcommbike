<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113467793-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-113467793-4');
    </script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Registrarse - CommBike - Tu aplicación de movildiad rápida y sostenible.</title>
    <link rel="preconnect" href="https://www.google-analytics.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
    <link rel="preconnect" href="https://www.googletagmanager.com" crossorigin>
    <meta name="msapplication-TileColor" content="#206bc4"/>
    <meta name="theme-color" content="#206bc4"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="MobileOptimized" content="320"/>
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon"/>
    <meta name="description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!"/>
    <meta name="twitter:image:src" content="https://preview.tabler.io/static/og.png">
    <meta name="twitter:site" content="@tabler_ui">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta name="twitter:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <meta property="og:image" content="https://preview.tabler.io/static/og.png">
    <meta property="og:image:width" content="1280">
    <meta property="og:image:height" content="640">
    <meta property="og:site_name" content="Tabler">
    <meta property="og:type" content="object">
    <meta property="og:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta property="og:url" content="https://preview.tabler.io/static/og.png">
    <meta property="og:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <!-- CSS files -->
    <link href="./css/tabler.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-flags.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-payments.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/tabler-vendors.min.css?1616362010" rel="stylesheet"/>
    <link href="./css/demo.min.css?1616362010" rel="stylesheet"/>
    <link href="css/misDatosVista.css" rel="stylesheet"/>
    
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    
    <script rel="javascript" type="text/javascript" href="js/jquery.min.js"></script>
    <script rel="javascript" type="text/javascript" href="js/jquery3.min.js"></script>
    
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>

  <script type="text/javascript">//SCRIPT PARA VISUALIZAR LA CONTRASEÑA COMO TEXTO Y CAMBIO ICONO OJO
    function mostrarPassword(){
      var cambio = document.getElementById("txtPassword");
      if(cambio.type == "password"){
        cambio.type = "text";
        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
      }else{
        cambio.type = "password";
        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
      }
    } 
  </script>
  <script>
  function doSomething() {
	  console.log("boton pulsado");		
		var person = {
	            nombre: document.getElementById("nombre").value,
	            correo: document.getElementById("email").value,
	            password: document.getElementById("txtPassword").value
	        }

	        $.ajax({
	            url: '/isst-commbike/api/usuarios/nuevoUsuario',
	            type: 'post',
	            dataType: 'json',
	            contentType: 'application/json',
	            success: function (data) {
	                console.log("exito");
	            },
	            data: JSON.stringify(person)
	        });
  }
  </script>
  
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="page page-center">
      <div class="container-tight py-4">
        <div class="text-center mb-4">
            <a href="."><img src="images/logo.jpg" height="36" alt=""></a>
        </div>
        <form id="my_form" class="card card-md" onsubmit="return doSomething();" method="post">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Crear nueva cuenta</h2>
            <div class="mb-3">
              <label class="form-label required">Nombre</label>
              <input type="text" id="nombre" name="nombre" class="form-control" placeholder="Introducir nombre">
            </div>
            <div class="mb-3">
              <label class="form-label required">Correo</label>
              <input type="email" id="email" name="email" class="form-control" placeholder="Introducir correo">
            </div>
            <div class="mb-3">
              <label class="form-label required">Contraseña</label>
              <div class="input-group input-group-flat">   
                <div class="input-group">
                  <input id="txtPassword" name="txtPassword" type="password" Class="form-control" placeholder="Introducir contraseña"  autocomplete="off">
                  <div class="input-group-append">
                    <button id="show_password" class="btn" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                  </div>
                </div>
              </div>

            </div>
            <div class="mb-3">
              <label class="form-check ">
                <input type="checkbox" class="form-check-input"/>
                <span class="form-check-label required">Acepto la <a href="./terms-of-service.html" tabindex="-1"> política de privacidad</a>.</span>
              </label>
            </div>
            <div class="form-footer">
              <button type="submit" class="btn btn-primary w-100">Crear nueva cuenta</button>
            </div>
          </div>
        </form>
        <div class="text-center text-muted mt-3">
          ¿Ya tienes una cuenta? <a href="./login.jsp" tabindex="-1">Iniciar sesión</a>
        </div>
      </div>
    </div>
    <script
	src="https://browser.sentry-cdn.com/5.27.6/bundle.tracing.min.js"
	integrity="sha384-9Z8PxByVWP+gIm/rTMPn9BWwknuJR5oJcLj+Nr9mvzk8nJVkVXgQvlLGZ9SIFEJF"
	crossorigin="anonymous"
></script>
    <script>
      Sentry.init({
      	dsn: "https://8e4ad02f495946f888620f9fb99fd495@o484108.ingest.sentry.io/5536918",
      	release: "tabler@1.0.0-beta",
      	integrations: [
      		new Sentry.Integrations.BrowserTracing()
      	],
      
      	tracesSampleRate: 1.0,
      });
    </script>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="./js/tabler.min.js?1616362010"></script>
  </body>
</html>