<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Mis datos - CommBike</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
    
    <link href="css/misDatosVista.css" rel="stylesheet"/>
    
    <link href="css/miHistorial.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
        
    <script rel="javascript" type="text/javascript" href="js/jquery.min.js"></script>
    <script rel="javascript" type="text/javascript" href="js/jquery3.min.js"></script>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  </head>
  <header class="navbar navbar-expand-md navbar-dark d-print-none">
  	<div class="container-xl">
      <jsp:include page="menu.jsp" flush="true" />
    </div>
  </header>
	
	<script type="text/javascript">
	function mostrarPassword3(){
	    var cambio = document.getElementById("txtPassword3");
	    if(cambio.type == "password"){
	      cambio.type = "text";
	      $('.icon3').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
	    }else{
	      cambio.type = "password";
	      $('.icon3').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
	    }
	  } 
	</script>
	<script type="text/javascript">
	function mostrarPassword1(){
	    var cambio = document.getElementById("txtPassword1");
	    if(cambio.type == "password"){
	      cambio.type = "text";
	      $('.icon1').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
	    }else{
	      cambio.type = "password";
	      $('.icon1').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
	    }
	  } 
	</script>
	<script type="text/javascript">
	function mostrarPassword2(){
	    var cambio = document.getElementById("txtPassword2");
	    if(cambio.type == "password"){
	      cambio.type = "text";
	      $('.icon2').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
	    }else{
	      cambio.type = "password";
	      $('.icon2').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
	    }
	  }
	</script>

	<script>
	  function funciontarjeta() {
		  console.log("registrar iban pulsado");
		  	var person = {
					correo: document.getElementById("email1").value,
					tarjeta: document.getElementById("tarjeta").value
		        }
	
		        $.ajax({
		            url: '/isst-commbike/api/usuarios/nuevaTarjeta',
		            type: 'put',
		            dataType: 'json',
		            contentType: 'application/json',
		            success: function (data) {
		                console.log("exito");
		            },
		            data: JSON.stringify(person)
		        });
	  }
	</script>

	<script
	src="https://browser.sentry-cdn.com/5.27.6/bundle.tracing.min.js"
	integrity="sha384-9Z8PxByVWP+gIm/rTMPn9BWwknuJR5oJcLj+Nr9mvzk8nJVkVXgQvlLGZ9SIFEJF"
	crossorigin="anonymous"></script>
    <script>
      Sentry.init({
      	dsn: "https://8e4ad02f495946f888620f9fb99fd495@o484108.ingest.sentry.io/5536918",
      	release: "tabler@1.0.0-beta",
      	integrations: [
      		new Sentry.Integrations.BrowserTracing()
      	],
      
      	tracesSampleRate: 1.0,
      });
    </script>
    
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="container-tight py-6">
      <form class="card card-md" action="." method="get" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Mis datos</h2>
          <div class="mb-3">
            <label class="form-label required">Contraseña actual</label>
            <div class="input-group">
                <input ID="txtPassword1" type="Password" Class="form-control" placeholder="Contraseña actual"  autocomplete="off">
                <div class="input-group-append">
                  <button id="show_password1" class="btn btn-primary" type="button" onclick="mostrarPassword1()"> 
                  	<span class="fa fa-eye-slash icon1"></span> 
                  </button>
                </div>
              </div>
          </div>
          <div class="mb-2">
            <label class="form-label required"> Contraseña nueva </label>
            <div class="mb-2">
            <div class="input-group input-group-flat">
              <div class="input-group">
                <input ID="txtPassword2" type="Password" Class="form-control" placeholder="Contraseña nueva"  autocomplete="off">
                <div class="input-group-append">
                  <button id="show_password2" class="btn btn-primary" type="button" onclick="mostrarPassword2()"> <span class="fa fa-eye-slash icon2"></span> </button>
                </div>
              </div>
            </div>
            </div>
          </div>
          <div class="mb-2">
            <label class="form-label required"> Repetir contraseña nueva </label>
            <div class="input-group input-group-flat">
              <div class="input-group">
                <input ID="txtPassword3" type="Password" Class="form-control" placeholder="Contraseña nueva"  autocomplete="off">
                <div class="input-group-append required">
                  <button id="show_password3" class="btn btn-primary" type="button" onclick="mostrarPassword3()"> <span class="fa fa-eye-slash icon3"></span> </button>
                </div>
              </div>
            </div>
          </div>
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Cambiar contraseña</button>
          </div>
        </div>
      </form>
    </div>
    <div class="container-tight py-6">
      <form class="card card-md" onsubmit="return funciontarjeta();" method="post" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Método de pago</h2>
          <div class="mb-3">
            <label class="form-label required href="./misDatos.jsp"">IBAN Cuenta bancaria</label>
            <input type="email" id="email1" name="email1" class="form-control" placeholder="Introducir correo asociado a la cuenta">
            <input type="text" id="tarjeta" name="tarjeta" class="form-control" placeholder="Introducir IBAN sin espacios ni guiones">
          </div>
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Registrar método de pago</button>
          </div>
        </div>
      </form>
      <div class="container-tight py-6">
        <form class="card card-md" action="." method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Mi historial</h2>
            	<table id="historial" border="1">
            		<thead>
	            		<tr>
				          <th scope="col" align='left' width="10%">Tiempo Uso</th>
				          <th scope="col" align='left' width="10%">Precio</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            	</tbody>
            	</table>
          </div>
        </form>
      </div>

	    
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
    <script src="//code.jquery.com/jquery-latest.js"></script>
    <script src="misDatosScript.js"></script>
  </body>
</html>