<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Inciar Sesión - CommBike - Tu aplicación de movildiad rápida y sostenible.</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
    <link href="css/misDatosVista.css" rel="stylesheet"/>
    

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    
  </head>
  <script>
/*   $(document).ready(function(){
	    $.ajax({
	        url: '/isst-commbike/api/login',
	        type: 'get',
	        data: {
                email: $("input[name=email]").val(),
                password: $("input[name=txtPassword]").val()
            },
	        dataType: 'JSON',
	        success: function(response){
	            var len = response.length;
	
	        }
	    });
	}); */
  </script>


  <script type="text/javascript">//SCRIPT PARA VISUALIZAR LA CONTRASEÑA COMO TEXTO Y CAMBIO ICONO OJO
    function mostrarPassword(){
      var cambio = document.getElementById("txtPassword");
      if(cambio.type == "password"){
        cambio.type = "text";
        $('.icon').removeClass('fa fa-eye-slash').addClass('fa fa-eye');
      }else{
        cambio.type = "password";
        $('.icon').removeClass('fa fa-eye').addClass('fa fa-eye-slash');
      }
    } 
  </script>

  
  
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
      <div class="container-tight py-6">
        <div class="text-center mb-4">
          <a href="."><img src="images/logo.jpg" height="36" alt=""></a>
        </div>
        <form class="card card-md" action="LoginServlet" method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Iniciar sesión</h2>
            <div class="mb-3">
              <label class="form-label required">Correo</label>
              <input type="email" name="email" id="email" class="form-control" placeholder="Introducir correo">
            </div>
            <div class="mb-2">
              <label class="form-label required">
                Password
                <span class="form-label-description">
                  <a href="#">He olvidado la contraseña</a>
                </span>
              </label>
              
              
              <div class="input-group input-group-flat">
                
                <div class="input-group">
                  <input id="txtPassword" name="txtPassword" type="Password" Class="form-control" placeholder="Introducir contraseña"  autocomplete="off">
                  <div class="input-group-append">
                    <button id="show_password" class="btn" type="button" onclick="mostrarPassword()"> <span class="fa fa-eye-slash icon"></span> </button>
                  </div>
                </div>

              </div>


            </div>
            <div class="mb-2">
              <label class="form-check">
                <input type="checkbox" class="form-check-input"/>
                <span class="form-check-label">Recordarme</span>
              </label>
            </div>
            
            
            
            <div class="form-footer">
	            <input type="button" value="input" class="btn btn-primary w-100">Iniciar sesión </input>
	        	<button type="submit" value="botton" class="btn btn-primary w-100">Iniciar sesión </button>
            </div>
            
          </div>
        </form>
        <div class="text-center text-muted mt-3">
          ¿Todavía no estás registrado? <a href="./registration.jsp" tabindex="-1">Regístrate</a>
        </div>
      </div>
    </div>
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
  </body>
</html>