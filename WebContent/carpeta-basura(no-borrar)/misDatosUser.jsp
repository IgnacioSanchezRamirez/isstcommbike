<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Mis datos - CommBike</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
    
    <link href="css/misDatosVista.css" rel="stylesheet"/>
    
    <link href="css/miHistorial.css" rel="stylesheet"/>
    <link href='https://fonts.googleapis.com/css?family=Rock+Salt' rel='stylesheet' type='text/css'>
  </head>
  <header class="navbar navbar-expand-md navbar-dark d-print-none">
    <div class="container-xl">
      <jsp:include page="MenuUser.jsp" flush="true" />
    </div>
  </header>

  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="container-tight py-6">
      <form class="card card-md" action="." method="get" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Mis datos</h2>
          <div class="mb-3">
            <label class="form-label required">Contraseña actual</label>
            <input type="email" class="form-control" placeholder="Introducir contraseña actual">
          </div>
          <div class="mb-2">
            <label class="form-label required"> Contraseña nueva </label>
            <div class="input-group input-group-flat">
              <input type="password" class="form-control"  placeholder="Contraseña nueva"  autocomplete="off">
              <span class="input-group-text">
                <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="2" /><path d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" /></svg>
                </a>
              </span>
            </div>
          </div>
          <div class="mb-2">
            <label class="form-label required"> Repetir contraseña nueva </label>
            <div class="input-group input-group-flat">
              <input type="password" class="form-control"  placeholder="Contraseña nueva"  autocomplete="off">
              <span class="input-group-text">
                <a href="#" class="link-secondary" title="Show password" data-bs-toggle="tooltip"><svg xmlns="http://www.w3.org/2000/svg" class="icon" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="currentColor" fill="none" stroke-linecap="round" stroke-linejoin="round"><path stroke="none" d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="12" r="2" /><path d="M22 12c-2.667 4.667 -6 7 -10 7s-7.333 -2.333 -10 -7c2.667 -4.667 6 -7 10 -7s7.333 2.333 10 7" /></svg>
                </a>
              </span>
            </div>
          </div>
          
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Cambiar contraseña</button>
          </div>
        </div>
      </form>
    </div>
    <div class="container-tight py-6">
      <form class="card card-md" action="." method="get" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Método de pago</h2>
          <div class="mb-3">
            <label class="form-label required">IBAN Cuenta bancaria</label>
            <input type="email" class="form-control" placeholder="Introducir IBAN sin espacios ni guiones">
          </div>
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Registrar método de pago</button>
          </div>
        </div>
      </form>
    </div>

      <div class="container-tight py-6">
        <form class="card card-md" action="." method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Mi historial</h2>
            	<table>
            		<thead>
	            		<tr>
	            			<th scope="col">Fecha</th>
	            			<th scope="col">Tiempo Uso</th>
	            			<th scope="col">Precio</th>
	            		</tr>
	            	</thead>
	            	<tbody>
	            		<tr class="blanco">
	            			<td>dd/mm/aaaa</td>
	            			<td>x minutos</td>
	            			<td>x €</td>
	            		</tr>
	            		<tr class="verde">
	            			<td>dd/mm/aaaa</td>
	            			<td>x minutos</td>
	            			<td>x €</td>
	            		</tr>
	            		<tr class="blanco">
	            			<td>dd/mm/aaaa</td>
	            			<td>x minutos</td>
	            			<td>x €</td>
	            		</tr>
	            		<tr class="verde">
	            			<td>dd/mm/aaaa</td>
	            			<td>x minutos</td>
	            			<td>x €</td>
	            		</tr>
	            	</tbody>
            	</table>
          </div>
        </form>
      </div>
    
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
  </body>
</html>