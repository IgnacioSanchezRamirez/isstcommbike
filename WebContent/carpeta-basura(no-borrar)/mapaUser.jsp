<%@ page contentType="text/html; charset=UTF-8"%>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
<head>
<meta charset="utf-8" />
<meta name="viewport"
	content="width=device-width, initial-scale=1, viewport-fit=cover" />
<meta http-equiv="X-UA-Compatible" content="ie=edge" />
<title>Mis datos - CommBike</title>
<!-- CSS files -->
<link href="css/tabler.min.css" rel="stylesheet" />
<link href="css/tabler-flags.min.css" rel="stylesheet" />
<link href="css/tabler-payments.min.css" rel="stylesheet" />
<link href="css/tabler-vendors.min.css" rel="stylesheet" />
<link href="css/demo.min.css" rel="stylesheet" />
</head>
<header class="navbar navbar-expand-md navbar-dark d-print-none">
	<div class="container-xl">
		<jsp:include page="/menu.jsp" flush="true" />
	</div>
</header>

<body
	class="antialiased border-top-wide border-primary d-flex flex-column">
	<div id="demo"></div>
	<div id="mapholder"></div>
	<script src="http://maps.google.com/maps/api/js?sensor=false"></script>

	<script type="text/javascript">
	cargarmap()
    var x = document.getElementById("demo");
    function cargarmap() {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
        function showPosition(position) {
            lat = position.coords.latitude;
            lon = position.coords.longitude;
            latlon = new google.maps.LatLng(lat, lon)
            mapholder = document.getElementById('mapholder')
            mapholder.style.height = '250px';
            mapholder.style.width = '500px';
            var myOptions = {
                center: latlon, zoom: 10,
                mapTypeId: google.maps.MapTypeId.ROADMAP,
                mapTypeControl: false,
                navigationControlOptions: { style: google.maps.NavigationControlStyle.SMALL }
            };
            var map = new google.maps.Map(document.getElementById("mapholder"), myOptions);
            var marker = new google.maps.Marker({ position: latlon, map: map, title: "You are here!" });
        }
        function showError(error) {
            switch (error.code) {
                case error.PERMISSION_DENIED:
                    x.innerHTML = "Denegada la peticion de Geolocalización en el navegador."
                    break;
                case error.POSITION_UNAVAILABLE:
                    x.innerHTML = "La información de la localización no esta disponible."
                    break;
                case error.TIMEOUT:
                    x.innerHTML = "El tiempo de petición ha expirado."
                    break;
                case error.UNKNOWN_ERROR:
                    x.innerHTML = "Ha ocurrido un error desconocido."
                    break;
            }
        }
    }
</script>





	<!-- Libs JS -->
	<!-- Tabler Core -->
	<script src="js/tabler.min.js"></script>
</body>
</html>