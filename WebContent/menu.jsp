
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-113467793-4"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      
      gtag('config', 'UA-113467793-4');
    </script>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Menu</title>
    <link rel="preconnect" href="https://www.google-analytics.com" crossorigin>
    <link rel="preconnect" href="https://fonts.googleapis.com" crossorigin>
    <link rel="preconnect" href="https://www.googletagmanager.com" crossorigin>
    <meta name="msapplication-TileColor" content="#206bc4"/>
    <meta name="theme-color" content="#206bc4"/>
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent"/>
    <meta name="apple-mobile-web-app-capable" content="yes"/>
    <meta name="mobile-web-app-capable" content="yes"/>
    <meta name="HandheldFriendly" content="True"/>
    <meta name="MobileOptimized" content="320"/>
    <link rel="icon" href="./favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="./favicon.ico" type="image/x-icon"/>
    <meta name="description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!"/>
    <meta name="twitter:image:src" content="https://preview.tabler.io/static/og.png">
    <meta name="twitter:site" content="@tabler_ui">
    <meta name="twitter:card" content="summary">
    <meta name="twitter:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta name="twitter:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <meta property="og:image" content="https://preview.tabler.io/static/og.png">
    <meta property="og:image:width" content="1280">
    <meta property="og:image:height" content="640">
    <meta property="og:site_name" content="Tabler">
    <meta property="og:type" content="object">
    <meta property="og:title" content="Tabler: Premium and Open Source dashboard template with responsive and high quality UI.">
    <meta property="og:url" content="https://preview.tabler.io/static/og.png">
    <meta property="og:description" content="Tabler comes with tons of well-designed components and features. Start your adventure with Tabler and make your dashboard great again. For free!">
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
  </head>
  
  <body class="antialiased border-top-wide border-primary d-flex flex-column">
  <div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="mb-3">
    <header class="navbar navbar-expand-md navbar-dark d-print-none">
      <div class="container-xl">
        
        <div class="navbar-nav flex-row order-md-last"></div>

        <div class="collapse navbar-collapse" id="navbar-menu">
          <div class="d-flex flex-column flex-md-row flex-fill align-items-stretch align-items-md-center">
            <ul class="navbar-nav">
				<div class="nav-item">
                  <p>Bienvenido: ${usuario2.nombre} , ${usuario.correo} , ${us.correo} ${usuario.correo} ${correo} ${nombre} ${nombre2}</p>
                </div>
				<div class="nav-item">
                  <a class="nav-link" href="./mapa.jsp" >
                    <span class="nav-link-title">
                      Mapa
                    </span>
                  </a>
                </div>
                
				<!-- La pesta�a Escanear QR solo le debe aparecer a los usuarios-->
                <div class="nav-item">
                  <a class="nav-link" href="./escanearQR.jsp" >
                    <span class="nav-link-title">
                      Escanear QR
                    </span>
                  </a>
                </div>
              
				<!-- La pesta�a gestionar bicicletas solo le debe aparecer a los admin-->
                <div class="nav-item">
                  <a class="nav-link" href="./gestionarBicicletas.jsp" >
                    <span class="nav-link-title">
                      Gestionar bicicletas
                    </span>
                  </a>
                </div>
              
              
              
              <div class="nav-item">
                  <a class="nav-link" href="./misDatos.jsp" >
                    <span class="nav-link-title">
                      Mis datos
                    </span>
                  </a>
              </div>
              
              
              <div class="nav-item">
                <a class="nav-link" href="./login.jsp" >
                  <span class="nav-link-title">
                    Cerrar Sesion
                  </span>
                </a>
              </div>
              
            </ul>
          </div>
        </div>
      </div>
    </header>
  </div>

</div>
</body>
  
</html>