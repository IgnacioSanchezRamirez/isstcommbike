<%@ page contentType="text/html; charset=UTF-8" %>
<!doctype html>
<!--
* Tabler - Premium and Open Source dashboard template with responsive and high quality UI.
* @version 1.0.0-beta
* @link https://tabler.io
* Copyright 2018-2021 The Tabler Authors
* Copyright 2018-2021 codecalm.net Paweł Kuna
* Licensed under MIT (https://github.com/tabler/tabler/blob/master/LICENSE)
-->
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge"/>
    <title>Gestionar bicicletas - CommBike</title>
    <!-- CSS files -->
    <link href="css/tabler.min.css" rel="stylesheet"/>
    <link href="css/tabler-flags.min.css" rel="stylesheet"/>
    <link href="css/tabler-payments.min.css" rel="stylesheet"/>
    <link href="css/tabler-vendors.min.css" rel="stylesheet"/>
    <link href="css/demo.min.css" rel="stylesheet"/>
    
    <link href="css/misDatosVista.css" rel="stylesheet"/>
    
  </head>
  <header class="navbar navbar-expand-md navbar-dark d-print-none">
    <div class="container-xl">
      <jsp:include page="menu.jsp" flush="true" />
    </div>
  </header>

  <body class="antialiased border-top-wide border-primary d-flex flex-column">
    
    <div class="flex-fill d-flex flex-column justify-content-center py-4">
    <div class="container-tight py-6">
      <form class="card card-md" action="." method="get" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Dar de alta una bicicleta</h2>
          <div class="mb-2">
            <button type="submit" class="btn btn-primary w-100">Añadir bicicleta</button>
          </div>
          <div class="mb-3">
            <label class="form-label">Id bicicleta nueva: </label>
            <input type="text" class="form-control" name="idAsignado" placeholder="Readonly..." value="Id asignado" readonly="">
          </div>
        </div>
      </form>
    </div>
    
    <div class="container-tight py-6">
      <form class="card card-md" action="." method="get" autocomplete="off">
        <div class="card-body">
          <h2 class="card-title text-center mb-4">Dar de baja una bicicleta</h2>
          <div class="mb-3">
            <label class="form-label required">Id bicicleta</label>
            <input type="number" class="form-control" placeholder="Introducir Id de la bicicleta para dar de baja">
          </div>
          <div class="form-footer">
            <button type="submit" class="btn btn-primary w-100">Eliminar bicicleta</button>
          </div>
        </div>
      </form>
    </div>

      <div class="container-tight py-6">
        <form class="card card-md" action="." method="get" autocomplete="off">
          <div class="card-body">
            <h2 class="card-title text-center mb-4">Modificar bicicleta</h2>
            <div class="mb-3">
            	<label class="form-label required">Id bicicleta</label>
            	<input type="number" class="form-control" placeholder="Introducir Id de la bicicleta para modificar">
          	</div>
          	<div class="mb-3">
            	<div class="mb-3">
                            <label class="form-label">Estado de la bicicleta</label>
                            <select type="text" class="form-select" placeholder="Select a date" id="select-tags-advanced" value="">
                              <option value="Taller">Taller</option>
                              <option value="Lista">Lista</option>
                              <option value="Reubicar">Reubicar</option>
                              <option value="Averiada">Averiada</option>
                            </select>
                </div>
            	
			</div>
			
			<div class="mb-2">
            	<button type="submit" class="btn btn-primary w-100">Modificar bicicleta</button>
          	</div>
          
          </div>
        </form>
      </div>
    
    
    <!-- Libs JS -->
    <!-- Tabler Core -->
    <script src="js/tabler.min.js"></script>
  </body>
</html>