$(document).ready(function(){
    $.ajax({
        url: '/isst-commbike/api/alquiler',
        type: 'get',
        dataType: 'JSON',
        success: function(response){
            var len = response.length;
            for(i=0; i<len; i++){
                var tiempoUso = response[i].horaFin - response[i].horaInicio;
                var precio = response[i].precio;

                var tr_str = "<tr>" +
            
                    "<td align='center'>" + tiempoUso + "</td>" +
                    "<td align='center'>" + precio + "</td>" +
                    "</tr>";
               

                $("#historial tbody").append(tr_str);
            }
        }
    });
});

